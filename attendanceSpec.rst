WPERP Attendance Plugin Modifications
=====================================

Functional Specifications
=========================

:Author: Talha Ahmed 
:Contact: talha.ahmed@gmail.com
:Date: Wed 05/30/2018 
:Copyright: ICE Animations Pvt. Ltd. All Rights Reserved.


Overview
--------

This document describes modifications to the Attendance Plugin as provided by
the creators of WPERP.

The existing attendance plugin lacks some features mainly relating to
reporting that must be implemented before it comes in mainstreams use in ICE
Animations.

This description is not intended to be complete at this time. The
specifications maybe revised after further discussion.


Current Features
----------------

The Parent WPERP Wordpress plugin is an open source project which contains the
basic HRM, CRM, Accounting and Project Management modules. 

The Home Page of the project is:

    https://wperp.com/

The functionality of the basic open source free plugin can be enhanced by
buying add-ons. One such add-on is the Attendance Module. The current features
of the attendance plugin can be seen on the following location:

    https://wperp.com/docs/hrm-add-ons/attendance-management

The reader is encouraged to refer to the given link to read about the various
existing features of the plugin.

We can categorize the interfaces in this plugin into three categories which
are pluggable into the parent WPERP Wordpress plugin.

a. **Settings**: The interface where global plugin settings are specified by
   the user for this plugin as well as for WPERP and other plugins.
b. **Reporting**: The page where attendance reports can be viewed and
   downloaded. These are accessible from the employee page for individual
   based reports and also from the Reporting Menu of the WPERP HRM Module.
c. **Main Dashboard Menu**: The items accessible from the menu added in the main
   dashboard where plugin functionalities can be accessed.

This document proposes implementation of a child plugin which adds to the
existing plugin features along with the Leave Management features to
accommodate requirements of policies of ICE Animations.

Scenarios
---------

The following are additional scenarios which should be handled by the plugin
when added to the Wordpress installation alongside WPERP and Attendance Module. 

Scenario 1: Unresolved Absences *Report*
````````````````````````````````````````

Mnal an HR Executive wants to keep an eye on who missed office on what day.
She wishes that there would be an option in the reporting feature of the ERP
where she can know the list of employees that were absent within a range of
dates. She needs to see when an employee skips work on a working day. She does
not care when someone doesn't come on a Sunday, on a Holiday or an Optional
day. She does not also want to see any days for which a leave has also been
approved. Each consecutive set of unresolved absence by a person should occupy
a single row in this tabular presentation.

To do all this manually is too much work for Mnal. Mnal does not like work.
She is a millennial.

Scenerio 2: Personal Attendance *Report*
````````````````````````````````````````

Bilal is a boss not always worried about the regularity and punctuality of his
subjects. But, He has got an employee, Talha, who is rumoured to never come on
time and also be absent on many days. Bilal wants to confirm this complaint so
he goes to Talha's profile on the ERP. In the attendance tab he can find his
**day by day attendance** in a table which he can **filter** by a range of dates. For
each day it mentions his presence status i.e. whether he was absent, on leave,
or present and his time status whether he was late, on half day, on time or if
he left early.  On the row on the bottom he can find a **summary** which sums of
all these things. He **downloads** this data down in a **spreadsheet**. Now he can
send an email with a strongly worded warning to Talha with data attached to it
about his attendance.

Scenario 3: Monthly Attendance *Report*
```````````````````````````````````````

It is that time of the month and Hassan, is an HR Executive, needs to generate
an attendance report. If he is not quick enough, Salaries will get delayed and
disgruntled employees will keep bugging him till the end of the world. He
wishes that he could go to the report section and use the ERP to generate a
report for the days attended for all active employees. He can **filter** this data
using a range of **dates**, or **department**. Each row shows him the number of days
attended, leaves availed, lates and early leaves for each active employee.
This row should be similar to the summary row in a personal attendance report.
Each row also has a link that leads him to a personal attendance report of the
employee. He uses the **download** feature of this system to get a **spreadsheet** of
the data which he can share with payroll.

Scenario 4: Timing / Over Time *Report*
```````````````````````````````````````

Muqeet is our finance geek. He has been given the task to determine which
employees have worked longer hours so they can be compensated for it in money
or time. Our finance geek, however, is not much into databases. When he wants
this data he wants to go to the ERP and view a report in table form which
simply delivers him the information of **Overtime** (or Under time) for each
employee in one row. In each row there is also a **link** which can lead him to
the employee's personal attendance report. He is also able to **download** this
data to a **spreadsheet** and dispense the payments to the needy workers who
have been bugging him about this for some time.

Scenario 5: Timing - Late / Early Leaves *Report*
`````````````````````````````````````````````````

Mnal the HR Executive is asked to take action against employees who arrive
late and / or leave early. She decides to charge penalties to all the late
comers and use the money for a grand office party. She goes to the Reports
section and filters the lates, early leaves, half days data in a given date
range for each employee. Each row of the data **links** back to the employee in
question's personal attendance report. She can also download this data into a
spreadsheet for use in calculation or correspondence.

Mnal's great idea for a party gives the employees a chance to bond with each
other and also solves the office tardiness problem.


Non Goals
---------

:Payroll functions: This specifications does not handle payroll functions.
                    Just concerns itself with specifications related to
                    attendance, which can further be used in payroll in a
                    separate process.

:Other HR functions: Handling Hiring / Termination of employees,
                     Specifications of policies. Leave application functions.

:Recording of Attendance Data: The attendance data is recorded using existing
                               functionality or syncing it with finger print
                               data. This specification does not dictate
                               implementation of record collection.


Details
-------

Definitions:
````````````

:Working Day: A day of the year which is not part of the weekend or is not
              included in public holidays for the year.

:Unresolved Absence: A working day when the employee failed to show up for
                     work while there was no approved leave for that day.

:Overtime: When an employee works beyond the specified in and out times. The
           extra hours are reported as overtime. If an employee shows up for
           work on a holiday, all his ours worked on that specific day are
           reported as overtime. According to ICE Animations policies, the
           employee is entitled to compensate overtime as holidays within
           three months of acquiring the overtime. Negative Overtime means
           **Under time**.

:Late: An employee is considered late when his in time of the day is later
       than office start time by a difference more than that of the grace
       period in minutes.


Report Fields:
``````````````

Summary Report Fields
~~~~~~~~~~~~~~~~~~~~~
The Following fields should be part of Summary Rows for each employee:

Employee Info
"""""""""""""
* Employee Name
* Employee Code
* Department
* Designation

Counts
""""""
* Total Days (In Specified Range)
* Presents
* Holidays
* Absents

Overtimes/Lates (Cumulative)
""""""""""""""""""""""""""""
* Over Time (+/-)(hrs/days)
* Lates (#)
* Early Leaves (#)
* Half Days (#)

Leaves
""""""
* Casual Leaves
* Sick Leaves
* Annual Leaves
* Marriage Leaves
* Leave w/o pay


Leaves Remaining
""""""""""""""""
* Casual Leaves Remaining
* Sick Leaves Remaining
* Annual Leaves Remaining



Day Attendance Fields:
~~~~~~~~~~~~~~~~~~~~~~

* Employee Name
* Employee Code
* Department
* Designation
* Date
* Day Of The Week
* Holiday Status 
* In Time
* Out Time
* Attendance Status ( P / A / Leave Type ))
* working Hours
* Overtime (hrs)
* Late (Y/N)
* Half Day (Y/N)
* Early Leave (Y/N)


Technical Requirements:
```````````````````````

* The Plugin should work on the latest available release of Wordpress
* The Plugin should integrate with the Latest Available Versions of WPERP,
  WPERP Attendance plugin
* The Plugin should be implemented as a child plugin of sorts requiring
  installation of both WPERP and attendance Plugins.
* The Plugin should implement the naming conventions used in Wordpress, WPERP
  and accompanying plugins in code and in the database.


Unaddressed Issues
------------------

This document at this moment does not address the following issues that are
part of Attendance policies of ICE Animations. 

:Recording Night Stays: The existing plugin is only capable of holding
                        information about the first In and last Out entry per
                        employee per day. This clouds the information if an
                        employee stays the night, two nights in a row.

:Recording Day Breaks: Since, the existing database only designed to hold
                       first In and Last Out information per employee per day.
                       It is not able to record if an employee takes an
                       unwarranted long break in the *middle* of the day.

:Optional Days: If an employee does not show up for work on an optional day
                that days is marked as a holiday. If an employee does show up
                work on an optional day, she is not awarded any overtime for
                work done on that day within the normal working hours.
                Saturdays are currently as optional days at ICE Animations.

:Shift Management: Ice Animations have employees which work in night (and day)
                   shifts such as admin and IT staff. Apart from these few
                   employees all the rest of the office works during the day.
                   Moreover, the office timing can differ based on
                   departments. The current shift management system does not
                   allow for this fine level of control, barring having to
                   manage shifts manually.

:Compensations: Overtime accumulated over days can be compensated in form of
                off-days to the employee, a special leave called
                "compensation" is granted to the employee which remain
                available until a period of three months when they can be
                availed subject to approval.

